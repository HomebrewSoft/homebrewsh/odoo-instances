# Odoo Instances

Tools to control Odoo instances from bash.


## instance-new
`instance-new INSTANCE VERSION`

### Synopsis
Creates and execute a new Odoo instance with his own DB instance, creates a folder in the `$HOME/dockers` dir with a `addons` dir, `config/odoo.conf` and `docker-compose.yml` files.

The instance is running in the `8069` port and use the `config/odoo.conf`, by default all the Community addons are present and the `addons` dir is used to load custom modules.

Inside the project's dir is a `backups` dir where will be the automated backups located (**WIP**).

The `docker-compose.yml` file is the master file to control de docker containers.

### ARGS
```
INSTANCE
    Instance name.

VERSION[8.0, 9.0, 10.0, 11.0, 12.0, 13.0]
    Odoo version to use
```


## instance-stop
`instance-stop INSTANCE`

### Synopsis
Stops the `DB` and `WEB` containers of the `INSTANCE`

### ARGS
```
INSTANCE
    Instance name.
```


## instance-up
`instance-up INSTANCE`

### Synopsis
Starts the `DB` and `WEB` containers of the `INSTANCE`; if any change was made on `docker-compose.yml` this changes are applied.

### ARGS
```
INSTANCE
    Instance name.
```


## instance-restart
`instance-restart INSTANCE`

### Synopsis
Combine the `instance-stop` and `instance-up`.

### ARGS
```
INSTANCE
    Instance name.
```


## instance-rm
`instance-rm INSTANCE [CONFIRM]`

### Synopsis
Stops and remove definitely an instance, if confirm arg is passed the deletion is done without interactive confirmation.

### ARGS
```
INSTANCE
    Instance name.
CONFIRM[y, Y]
    If is passed, no need to confirm interactive
```


## instance-dev
`instance-dev INSTANCE [ARGS]`

### Synopsis
Executes an odoo in the same instance of the project but it will run on `8070` port and it will be interactive; all the extra args are passed directly to the odoo binary.

### ARGS
```
INSTANCE
    Instance name.
ARGS
    Arbitrary args to be passed to odoo binary.
```


## instance-attach
`instance-attach INSTANCE`

### Synopsis
Opens a interactive bash inside the `WEB` container of the instance using the `odoo` user.

### ARGS
```
INSTANCE
    Instance name.
```


## instance-exec
`instance-exec INSTANCE ARGS`

### Synopsis
Execute the args (interactively) inside the `WEB` container of the instance using the `odoo` user.

### ARGS
```
INSTANCE
    Instance name.
ARGS
    Arbitrary args to be passed to bash.
```


## instance-exec0
`instance-exec0 INSTANCE ARGS`

### Synopsis
Execute the args (interactively) inside the `WEB` container of the instance using the `root` user.

### ARGS
```
INSTANCE
    Instance name.
ARGS
    Arbitrary args to be passed to bash.
```


## instance-status
`instance-status [INSTANCE] [DB]`

### Synopsis
Return the status of the `WEB` or `DB`, if this arg is passed, of the instance container. If no args are passed it will show the status of all the instances

### ARGS
```
INSTANCE
    Instance name.

DB
    If is set, shows the status of the DB container instead of the WEB.
```


## instance-backup
~~`instance-backup [INSTANCE]`~~
**WIP**

### Synopsis
Create a ZIP with all the content of the instance: DB's, modules, configs.

### ARGS
```
INSTANCE
    Instance name.
```



## instance-restore
~~`instance-restore [INSTANCE]`~~
**WIP**

### Synopsis
Restore a instance from the a ZIP created using `instance-backup`.

### ARGS
```
INSTANCE
    Instance name.
```


## instance-zip
~~`instance-zip [INSTANCE] [URL]`~~
**WIP**

### Synopsis
Download and unzip a ZIP file from the web using the URL and it is puts on the `addons` folder.

### ARGS
```
INSTANCE
    Instance name.
URL
    URL of the ZIP file to e downloaded.
```

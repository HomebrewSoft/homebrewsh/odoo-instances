from subprocess import PIPE, Popen
import json
import hashlib
from flask import Flask, jsonify, request
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity

app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = 'super-secret'  # TODO Change this!
jwt = JWTManager(app)


def to_utf8(d):
    for k in d:
        if type(d[k]) == bytes:
            d[k] = d[k].decode('UTF-8')
    return d


def to_utf8_json(d):
    return json.dumps(to_utf8(d))


def standard_request_project_instance(command, request):
    get_jwt_identity()

    try:
        project = request.args['project']
        instance = request.args['instance']
    except KeyError:
        return to_utf8_json({'error': "'project' and 'instance' needed"}), 400

    with Popen([f'./{command}', project, instance], stdout=PIPE, stderr=PIPE) as proc:
        proc_res = proc.communicate()
        if proc.returncode:
            return to_utf8_json({'error': proc_res[1]}), 400
        else:
            return to_utf8_json({'response': proc_res[0]}), 201


@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400
    
    username = request.json.get('username', None)
    password = request.json.get('password', None)
    password_hashed = hashlib.sha3_512(password.encode()).hexdigest()
    archivo = open("hash.txt", "r")
    for hash_password in archivo.readlines():
        print (hash_password)
    archivo.close() 
    #hash_password = '4ed2b28324a9c0e2439b761a33397d4ad321e4e3e734312b1cb5ddc552501a3e24634ecae2612257118a81c60479cf6d9097b2a3593df8ad6a9ba819e5074e78'
    
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    if username != 'test' or password_hashed != hash_password:  # TODO
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token), 200


@app.route('/instance_status', methods=['GET'])
@jwt_required
def instance_status():
    get_jwt_identity()

    project = request.args.get('project', '')
    instance = request.args.get('instance', '')
    db = request.args.get('db', '')

    with Popen(["./instance-status", project, instance, db], stdout=PIPE, stderr=PIPE) as proc:
        proc_res = proc.communicate()
        status_string = proc_res[0].decode('UTF-8').replace('\n', '')
        return f'{{"instances": [{status_string}]}}', 200


@app.route('/project_new', methods=['GET'])
@jwt_required
def project_new():
    get_jwt_identity()

    try:
        project = request.args['project']
        version = request.args['version']
    except KeyError:
        return to_utf8_json({'error': "'project' and 'version' needed"}), 400

    with Popen([f'./project-new', project, version], stdout=PIPE, stderr=PIPE) as proc:
        proc_res = proc.communicate()
        if proc.returncode:
            return to_utf8_json({'error': proc_res[1]}), 400
        else:
            return to_utf8_json({'response': proc_res[0]}), 201


@app.route('/project_rm', methods=['GET'])
@jwt_required
def project_rm():
    get_jwt_identity()

    try:
        project = request.args['project']
    except KeyError:
        return to_utf8_json({'error': "'project' needed"}), 400

    with Popen([f'./project-rm', project], stdout=PIPE, stderr=PIPE) as proc:
        proc_res = proc.communicate()
        if proc.returncode:
            return to_utf8_json({'error': proc_res[1]}), 400
        else:
            return to_utf8_json({'response': proc_res[0]}), 201


@app.route('/instance_new', methods=['GET'])
@jwt_required
def instance_new():
    return standard_request_project_instance('instance-new', request)


@app.route('/instance_restart', methods=['GET'])
@jwt_required
def instance_restart():
    return standard_request_project_instance('instance-restart', request)


@app.route('/instance_rm', methods=['GET'])
@jwt_required
def instance_rm():
    return standard_request_project_instance('instance-rm', request)


@app.route('/instance_stop', methods=['GET'])
@jwt_required
def instance_stop():
    return standard_request_project_instance('instance-stop', request)


@app.route('/instance_up', methods=['GET'])
@jwt_required
def instance_up():
    return standard_request_project_instance('instance-up', request)


@app.route('/instance_cp', methods=['GET'])
@jwt_required
def instance_cp():
    get_jwt_identity()

    try:
        project = request.args['project']
        instance = request.args['instance']
        project_new = request.args['project_new']
        instance_new = request.args['instance_new']
        db = request.args.get('db')
    except KeyError:
        return to_utf8_json({'error': "'project', 'instance', 'project_new' and 'instance_new' are needed"}), 400

    with Popen([f'./instance-cp', project, instance, project_new, instance_new, db], stdout=PIPE, stderr=PIPE) as proc:
        proc_res = proc.communicate()
        if proc.returncode:
            return to_utf8_json({'error': proc_res[1]}), 400
        else:
            return to_utf8_json({'response': proc_res[0]}), 201


if __name__ == '__main__':
    app.run(host='0.0.0.0')  # TODO rem
